class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.stringbody :title

      t.timestamps
    end
  end
end
